package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

//Request is the struct which describing the format from client
//the whole struct should be stored in SMF body
type Request struct {
	//Param is the param in HTTP request
	//especially useful in GET request
	Param map[string]string `json:"param"`
	//Header is the header will be put on HTTP request
	Header map[string]string `json:"header"`
	//Body is the body will be put on HTTP request
	Body []byte `json:"body"`
}

//Response is the struct which will be returned to client
//the whole struct will be stored in SMF body
type Response struct {
	//Status represent the HTTP status code, eg 200,401
	Status int `json:"status"`
	//Header represent HTTP header return from server
	Header map[string]interface{} `json:"header"`
	//Body represent HTTP body return from server
	Body []byte `json:"body"`
}

// @Desc Build request message
func (o *Request) PackRequest(param, header map[string]string, msg interface{}) (requestBody []byte, err error) {

	body, err := json.Marshal(msg)

	if err != nil {
		return nil,	errors.Errorf(constant.REQPACKERR, "Build request message failed, [%++v]", err)
	}

	requestMsg := &constant.Request{
		Param: nil,
		Header: map[string]string{
			"Content-Type": "application/json",
		},
		Body: body,
	}

	requestBody, err = json.Marshal(requestMsg)

	if err != nil {
		return nil,	errors.Errorf(constant.REQPACKERR, "Build request message failed, [%++v]", err)
	}

	return requestBody, nil
}

// @Desc Parsing Response message
func (o *Response) UnPackResponse(responseBody []byte) error {

	if err := json.Unmarshal(responseBody, o); err != nil {
		return errors.Errorf(constant.RSPUNPACKERR, "UnPackResponse failed, %v", err)
	}

	return nil
}
