//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000014I struct {
	SourceFunds   string `json:"SourceFunds" validate:"required,max=1"`    //资金来源              1-客户账户；2-内部账户；3-合约
	PayAgrmt      string `json:"PayAgrmt" validate:"max=30"`               //付款合约号
	PayAgrmtTyp   string `json:"PayInAcc" validate:"max=5"`                //付款合约类型            10001-个人活期账户;10002-个人定期账户;10003-个人活期保证金账户;10004-个人定期保证金账户;10005-对公活期账户;10006-对公定期账户;10007-对公活期保证金账户;10008-对公定期保证金账户;10009-内部账户;20001-个人贷款账户;20002-对公贷款账户;30001-协议存款;30002-保证金;30003-同业存款;30004-智能通知存款;30005-法人账户透支;30006-集团账户;30007-协定存款;30008-收息账号;30009-活期存款转定期约定;30010-网银开户签约;30011-贷款;30012-贷款收费;30013-普通定期存款;30014-活期保证金存款;30015-定期保证金存款;30016-普通活期存款;40001-客户对账单;-
	PayInAcc      string `json:"PayInAcc" validate:"max=30"`               //转出内部户账号
	PayMdsNM      string `json:"PayMdsNM" validate:"max=30"`               //付款方介质号码
	PayMdsTyp     string `json:"PayMdsTyp" validate:"max=3"`               //付款方介质类型
	PayCusId      string `json:"PayCusId" validate:"max=20"`               //付款方客户编号
	PayCUR        string `json:"PayCUR" validate:"required,max=3"`         //付款方币别             156-人民币
	CashTranFlag  string `json:"CashTranFlag" validate:"required,max=2"`   //钞汇标志              C-钞户;T-汇户;-
	PayAccuntNme  string `json:"PayAccuntNme" validate:"required,max=200"` //付款账户名称
	UsgCod        string `json:"UsgCod" validate:"required,max=1"`         //用途代码              EAC-电子账号
	WdrwlMthd     string `json:"WdrwlMthd" validate:"max=1"`               //支取方式              1-密码;
	NeedPswFlg    string `json:"NeedPswFlg" validate:"required,max=1"`     //是否需要验密            Y-需要验密；N-不需要验密
	AccPsw        string `json:"AccPsw" validate:"max=1"`                  //账户密码              当是否需要验密为Y，本字段必填
	TranAmt       string `json:"TranAmt" validate:"required"`              //金额
	GoingFunds    string `json:"GoingFunds" validate:"required,max=1"`     //资金去向              1-客户账户；2-内部账户
	CltAgrmt      string `json:"CltAgrmt" validate:"required,max=30"`      //收款合约号
	CltAgrmtTyp   string `json:"CltAgrmtTyp" validate:"max=5"`             //收款合约类型
	CltInAcc      string `json:"CltInAcc" validate:"required,max=30"`      //转入内部户账号
	CltMdsNM      string `json:"CltMdsNM" validate:"max=30"`               //收款方介质号码
	CltMdsTyp     string `json:"CltMdsTyp" validate:"max=3"`               //收款方介质类型
	CltCusId      string `json:"CltCusId" validate:"max=20"`               //收款方客户编号
	CltCUR        string `json:"CltCUR" validate:"required,max=3"`         //收款方币别
	CltAccuntNme  string `json:"CltAccuntNme" validate:"max=200"`          //收款账户名称
	Postscript    string `json:"Postscript" validate:"max=200"`            //转账附言
	DlyArrvlAFlag string `json:"DlyArrvlAFlag" validate:"required,max=1"`  //延时到账标志            1-实时到账；2-延时到账
	TranTyp       string `json:"TranTyp" validate:"required,max=1"`        //交易方式              1-联机；2-批量
	AmtFreAuth    string `json:"AmtFreAuth" validate:"required,max=1"`     //金额冻结授权标志          Y-已经授权

}

type SSV1000014O struct {
	TueTranAmt float64 `json:"TueTranAmt"` //实际扣款金额
	ReturnCode string  `json:"ReturnCode"`
}

// @Desc Build request message
func (o *SSV1000014I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000014I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000014O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000014O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000014I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000014I) GetServiceKey() string {
	return "ssv1000014"
}
