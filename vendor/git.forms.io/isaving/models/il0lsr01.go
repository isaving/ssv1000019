package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type IL0LSR01I struct {
	SysId             string            `json:"sysId"`             //渠道标识 暂定ILNS 乐高智能贷款
	BizSn             string            `json:"bizSn"`             //业务流水号
	ObjectSendList    IL0LSR01IItemList `json:"objectSendList"`    //发送对象内容集合
	ObjectReceiveList IL0LSR01IItemList `json:"objectReceiveList"` //接收对象内容集合
	LoanProdtNo       string            `json:"loanProdtNo"`       //产品号
	ApplyScena        string            `json:"applyScena"`        //适用场景大类
	TextTmplTypCd     string            `json:"textTmplTypCd"`     //模板业务类型代码 1001-Transaction Statement 1002-Transaction History 1003-Repayment Plan 1004-Late Payment Notification
	OrgId             string            `json:"orgId"`             //机构id
	Area              string            `json:"area"`              //地区
	ChannelNumber     string            `json:"channelNumber"`     //渠道号
	EventId           string            `json:"eventId"`           //事件ID TopicID
	SendObjectType    string            `json:"sendObjectType"`    //发送对象类型（01-机构，02-客户经理）
	TextTmplDetailCd  string            `json:"textTmplDetailCd"`  //模板业务类型细类
	MsgDate           string            `json:"msgDate"`           //日期
	MsgTime           string            `json:"msgTime"`           //时间
	Body              map[string]interface{}            `json:"body"`              //内容
	AttachFileIDList  []string          `json:"attachFileIDList"`  //附件文件ID列表
}

type IL0LSR01O struct {
	ReturnCode   string `json:"returnCode"`   //错误码：0-邮件发送成功
	ReturnMsg    string `json:"returnMsg"`    //响应信息："SUCCESS"-发送成功
	DeliveryTime string `json:"deliveryTime"` //发件时间，格式：YYYYMMDDHH24MMSS
	ResponseTime string `json:"responseTime"` //响应时间，格式：YYYYMMDDHH24MMSS
	RepData      string `json:"repData"`      //邮件发送结果：true-成功，false-失败
}

type IL0LSR01IDataForm struct {
	FormHead CommonFormHead
	FormData IL0LSR01I
}

type IL0LSR01ODataForm struct {
	FormHead CommonFormHead
	FormData IL0LSR01O
}

type IL0LSR01RequestForm struct {
	Form []IL0LSR01IDataForm
}

type IL0LSR01ResponseForm struct {
	Form []IL0LSR01ODataForm
}

// @Desc Build request message
func (o *IL0LSR01RequestForm) PackRequest(IL0LSR01I IL0LSR01I) (requestBody []byte, err error) {

	requestForm := IL0LSR01RequestForm{
		Form: []IL0LSR01IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL0LSR01I001",
				},
				FormData: IL0LSR01I,
			},
		},
	}

	requestBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *IL0LSR01RequestForm) UnPackRequest(request []byte) (IL0LSR01I, error) {
	IL0LSR01I := IL0LSR01I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL0LSR01I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL0LSR01I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL0LSR01ResponseForm) PackResponse(IL0LSR01O IL0LSR01O) (responseBody []byte, err error) {
	responseForm := IL0LSR01ResponseForm{
		Form: []IL0LSR01ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL0LSR01O001",
				},
				FormData: IL0LSR01O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL0LSR01ResponseForm) UnPackResponse(request []byte) (IL0LSR01O, error) {

	IL0LSR01O := IL0LSR01O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL0LSR01O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL0LSR01O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (hs IL0LSR01IItemList) Len() int { //返回集合长度
	return len(hs)
}

func (hs IL0LSR01IItemList) Less(i, j int) bool { //返回布尔值
	return hs[i].SerialNo < hs[j].SerialNo
}

func (hs IL0LSR01IItemList) Swap(i, j int) { //交换
	hs[i], hs[j] = hs[j], hs[i]
}
