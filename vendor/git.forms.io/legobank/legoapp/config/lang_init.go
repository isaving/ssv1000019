package config

import (
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/beego/i18n"
	"github.com/pkg/errors"
	"path/filepath"
	"strings"
)

//Init i18n
func InitLocaleLang() error {

	langFiles, err := filepath.Glob("conf/locale_*.ini")

	if err != nil {
		return err
	}

	if len(langFiles) < 1 {
		return errors.New("Can't found locale file.")
	}

	log.Infof("Start init %++v language files.", langFiles)

	for _, langFile := range langFiles {
		lang := strings.ReplaceAll(strings.Split(langFile, "e_")[1], ".ini", "")

		if err := i18n.SetMessage(lang, langFile); err != nil {
			return err
		} else {
			log.Debugf("init %s successfully.", lang)
		}
	}

	log.Infof("Init I18n config successfully.")
	return nil
}
