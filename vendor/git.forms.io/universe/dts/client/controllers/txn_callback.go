//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package controllers

import (
	"fmt"
	jsoniter "github.com/json-iterator/go"

	//"encoding/json"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/common/event_handler/base"
	"git.forms.io/universe/common/event_handler/constant"
	"git.forms.io/universe/dts/client/callback"
	event "git.forms.io/universe/dts/common/event/transaction"
	"git.forms.io/universe/dts/common/log"
	"git.forms.io/universe/dts/common/util"
	"git.forms.io/universe/solapp-sdk/event/message"
)

// @Desc the controller to deal with call-back
type DTSCallbackController struct {
	base.BaseController
	ErrorCode int
}

// @Desc the Prepare() of DTSCallbackController, which finds out information from Request
func (c *DTSCallbackController) Prepare() {
	c.Req = &client.UserMessage{
		//message payload
		Body: c.Ctx.Input.RequestBody,
	}
}

// @Desc the Finish() of DTSCallbackController, which describes response according to transaction's result
func (c *DTSCallbackController) Finish() {
	txnMsg := &message.TxnMessage{}
	if nil != c.Rsp {
		txnMsg.Id = c.Rsp.Id
		txnMsg.TopicAttribute = c.Rsp.TopicAttribute
		txnMsg.NeedReply = c.Rsp.NeedReply
		txnMsg.NeedAck = c.Rsp.NeedAck
		txnMsg.AppProps = c.Rsp.AppProps

	}
	if nil == c.Rsp && c.Err != nil {
		errRes := base.CommonResponse{
			ErrorCode: c.ErrorCode,
			ErrorMsg:  c.Err.Error(),
		}
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		errResJson, _ := json.Marshal(errRes)
		txnMsg.Body = string(errResJson)
		c.Data[constant.MESSAGE_TYPE_JSON] = *txnMsg
	} else {
		txnMsg.Body = string(c.Rsp.Body)
	}
	c.Data[constant.MESSAGE_TYPE_JSON] = *txnMsg
	c.ServeJSON()
}

// @Desc return controller's name
func (c *DTSCallbackController) ControllerName() string {
	return "DTSCallbackController"
}

// @Desc locally executes confirm method
func (c *DTSCallbackController) AtomicTransactionConfirm() {
	json := jsoniter.ConfigCompatibleWithStandardLibrary

	defer func() {
		if r := recover(); r != nil {
			log.Errorf("AtomicTransactionConfirm catch panic, error=%++v", r)
			var res base.CommonResponse
			res = base.BuildErrorResponseWithErrorCode(-1, fmt.Sprintf("dts client confirm failed, error=%++v!", r))
			body, _ := json.Marshal(res)
			c.SetResponse(&client.UserMessage{
				AppProps: c.Req.AppProps,
				Body:     body,
			})
		}
	}()

	txnCallback := callback.NewTxnCallback()
	requestBody := c.GetRequest().Body
	request := event.AtomicTxnConfirmRequest{}
	if c.Err = json.Unmarshal(requestBody, &request); c.Err != nil {
		log.Errorf("Local transaction confirm: request message unmarshal failed, err=%v, request data=%v", c.Err, string(requestBody))
		return
	}

	var res base.CommonResponse

	if errorCode, err := txnCallback.Confirm(request.Request.RootXid, request.Request.BranchXid); err != nil {
		c.Err = err
		if 0 != errorCode {
			c.ErrorCode = errorCode
		} else {
			c.ErrorCode = -1
		}
		res = base.BuildErrorResponseWithErrorCode(c.ErrorCode, err.Error())

		log.Errorf("Atomic transaction confirm failed, error=%v", err)
	} else {
		response := event.AtomicTxnConfirmResponseBody{
			RootXid:      request.Request.RootXid,
			BranchXid:    request.Request.BranchXid,
			ResponseTime: util.CurrentTime(),
		}
		//requestJson, _ := json.Marshal(response)
		res = base.BuildResponse(response)
	}
	body, _ := json.Marshal(res)

	var appProps map[string]string
	if nil == c.Req || nil == c.Req.AppProps {
		appProps = make(map[string]string)
	} else {
		appProps = c.Req.AppProps
	}

	appProps["RetMsgCode"] = "0"
	appProps["RetMessage"] = ""
	c.SetResponse(&client.UserMessage{
		AppProps: appProps,
		Body:     body,
	})
}

// @Desc locally executes cancel method
func (c *DTSCallbackController) AtomicTransactionCancel() {
	json := jsoniter.ConfigCompatibleWithStandardLibrary

	defer func() {
		if r := recover(); r != nil {
			log.Errorf("AtomicTransactionCancel catch panic, error=%v", r)
			var res base.CommonResponse
			res = base.BuildErrorResponseWithErrorCode(-1, fmt.Sprintf("dts client cancel failed, error=%++v!", r))
			body, _ := json.Marshal(res)
			c.SetResponse(&client.UserMessage{
				AppProps: c.Req.AppProps,
				Body:     body,
			})
		}
	}()

	txnCallback := callback.NewTxnCallback()
	requestBody := c.GetRequest().Body
	request := event.AtomicTxnCancelRequest{}
	if c.Err = json.Unmarshal(requestBody, &request); c.Err != nil {
		log.Errorf("Local transaction cancel: request message unmarshal failed, err=%v, request data=%v", c.Err, string(requestBody))
		return
	}
	var res base.CommonResponse

	if errorCode, err := txnCallback.Cancel(request.Request.RootXid, request.Request.BranchXid); err != nil {
		c.Err = err
		if 0 != errorCode {
			c.ErrorCode = errorCode
		} else {
			c.ErrorCode = -1
		}
		res = base.BuildErrorResponseWithErrorCode(c.ErrorCode, err.Error())

		log.Errorf("Atomic transaction cancel failed, error=%++v", err)
	} else {
		response := event.AtomicTxnCancelResponseBody{
			RootXid:      request.Request.RootXid,
			BranchXid:    request.Request.BranchXid,
			ResponseTime: util.CurrentTime(),
		}
		res = base.BuildResponse(response)
	}
	body, _ := json.Marshal(res)

	var appProps map[string]string
	if nil == c.Req || nil == c.Req.AppProps {
		appProps = make(map[string]string)
	} else {
		appProps = c.Req.AppProps
	}

	appProps["RetMsgCode"] = "0"
	appProps["RetMessage"] = ""
	c.SetResponse(&client.UserMessage{
		AppProps: appProps,
		Body:     body,
	})
}
