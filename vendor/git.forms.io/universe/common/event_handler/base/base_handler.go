//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package base

import (
	"git.forms.io/universe/comm-agent/client"
	jsoniter "github.com/json-iterator/go"
)

type EventHandlerInterface interface {
	SetRequest(request client.UserMessage)
	GetRequest() client.UserMessage

	SetResponse(response *client.UserMessage)
	GetResponse() *client.UserMessage

	IsEventCallback() bool
	SetEventCallback()

	EventHandlePrepare()
	EventHandleFinish()
}

type BaseHandler struct {
	EventHandlerInterface
	Req           *client.UserMessage
	Rsp           *client.UserMessage
	Err           error
	EventCallback bool
}

func (b *BaseHandler) SetRequest(request client.UserMessage) {
	b.Req = &request
}

func (b *BaseHandler) GetRequest() client.UserMessage {
	return *b.Req
}

func (b *BaseHandler) EventHandlePrepare() {
	// empty logic
}

func (b *BaseHandler) SetEventCallback() {
	b.EventCallback = true
}

func (b *BaseHandler) IsEventCallback() bool {
	return b.EventCallback
}

func (b *BaseHandler) EventHandleFinish() {
	if nil == b.Rsp && b.Err != nil {
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		res, _ := json.Marshal(BuildErrorResponse(b.Err.Error()))

		var appProps map[string]string
		if nil != b.Req && nil != b.Req.AppProps {
			appProps = b.Req.AppProps
		} else {
			appProps = make(map[string]string)
		}

		b.Rsp = &client.UserMessage{
			AppProps: appProps,
			Body:     res,
		}
	}
}

func (b *BaseHandler) SetResponse(response *client.UserMessage) {
	b.Rsp = response
}

func (b *BaseHandler) GetResponse() *client.UserMessage {
	return b.Rsp
}

type CommonResponse struct {
	ErrorCode int //-1 indicat error ,0 indicat success
	ErrorMsg  string
	Data      interface{}
}

func BuildErrorResponse(errMsg string) CommonResponse {
	response := CommonResponse{
		ErrorCode: -1,
		ErrorMsg:  errMsg,
	}
	return response
}

func BuildErrorResponseWithErrorCode(errCode int, errMsg string) CommonResponse {
	response := CommonResponse{
		ErrorCode: errCode,
		ErrorMsg:  errMsg,
	}
	return response
}

func BuildResponse(data interface{}) CommonResponse {
	response := CommonResponse{
		ErrorCode: 0,
		Data:      data,
	}
	return response
}

type DataSet struct {
	Total int
	Datas interface{}
}

func BuildPagingResponse(total int, data interface{}) CommonResponse {
	dataSet := DataSet{
		Total: total,
		Datas: data,
	}
	return BuildResponse(dataSet)
}
