//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package padding

import (
	"bytes"
	"crypto/rand"
	"github.com/go-errors/errors"
	"io"
)

func PKCS5Padding(cipherText []byte, blockSize int) ([]byte, error) {
	padding := blockSize - len(cipherText)%blockSize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(cipherText, padText...), nil
}

func PKCS5UnPadding(originData []byte) []byte {
	return lengthUnPadding(originData)
}

func ZeroPadding(cipherText []byte, blockSize int) ([]byte, error) {
	padding := blockSize - len(cipherText)%blockSize
	padText := bytes.Repeat([]byte{0}, padding)
	return append(cipherText, padText...), nil
}

func ZeroUnPadding(origData []byte) []byte {
	return bytes.TrimFunc(origData,
		func(r rune) bool {
			return r == rune(0)
		})
}

func PKCS7Padding(cipherText []byte, blocksize int) ([]byte, error) {
	padding := blocksize - len(cipherText)%blocksize
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(cipherText, padText...), nil
}

func lengthUnPadding(originData []byte) []byte {
	length := len(originData)
	unpadding := int(originData[length-1])
	return originData[:(length - unpadding)]
}

func PKCS7UnPadding(originData []byte) []byte {
	return lengthUnPadding(originData)
}

// 填充序列的最后一个字节填paddingSize，其它填0。
func ANSIX923Padding(cipherText []byte, blockSize int) ([]byte, error) {
	padding := blockSize - len(cipherText)%blockSize

	padText := bytes.Repeat([]byte{0}, padding-1)
	padText = append(padText, byte(padding))

	return append(cipherText, padText...), nil
}

func ANSIX923UnPadding(originData []byte) []byte {
	return lengthUnPadding(originData)
}

// 填充序列的最后一个字节填paddingSize， 其它填随机数。
func ISO10126Padding(cipherText []byte, blockSize int) ([]byte, error) {
	padding := blockSize - len(cipherText)%blockSize
	padText := make([]byte, padding-1)

	if _, err := io.ReadFull(rand.Reader, padText); err != nil {
		return nil, errors.Wrap(err, 0)
	}
	padText = append(padText, byte(padding))

	return append(cipherText, padText...), nil
}

func ISO10126UnPadding(originData []byte) []byte {
	return lengthUnPadding(originData)
}

const (
	LENGTH_OF_AES_COMMON_IV      = 16
	LENGTH_OF_AES256_GCM_NONCE   = 12
	LENGTH_OF_DES_COMMON_IV      = 8
	LENGTH_OF_3DES_COMMON_IV     = 8
	LENGTH_OF_BLOWFISH_COMMON_IV = 8
)

func Padding(cipherText []byte, blockSize int, padding string) ([]byte, error) {
	var err error
	switch padding {
	case "PKCS5":
		cipherText, err = PKCS5Padding(cipherText, blockSize)
	case "ZERO":
		cipherText, err = ZeroPadding(cipherText, blockSize)
	case "PKCS7":
		cipherText, err = PKCS7Padding(cipherText, blockSize)
	case "ANSIX923":
		cipherText, err = ANSIX923Padding(cipherText, blockSize)
	case "ISO10126":
		cipherText, err = ISO10126Padding(cipherText, blockSize)
	}

	return cipherText, err
}

func Unpadding(cipherText []byte, padding string) []byte {
	switch padding {
	case "PKCS5":
		cipherText = PKCS5UnPadding(cipherText)
	case "ZERO":
		cipherText = ZeroUnPadding(cipherText)
	case "PKCS7":
		cipherText = PKCS7UnPadding(cipherText)
	case "ANSIX923":
		cipherText = ANSIX923UnPadding(cipherText)
	case "ISO10126":
		cipherText = ISO10126UnPadding(cipherText)
	}

	return cipherText
}

func generateAESIVOrNonce(cryptoMode string) ([]byte, error) {
	if "GCM" == cryptoMode {
		nonce := make([]byte, LENGTH_OF_AES256_GCM_NONCE)
		if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
			return nil, errors.Wrap(err, 0)
		}

		return nonce, nil
	} else {
		iv := make([]byte, LENGTH_OF_AES_COMMON_IV)

		if _, err := io.ReadFull(rand.Reader, iv); err != nil {
			return nil, errors.Wrap(err, 0)
		}

		return iv, nil
	}
}

func generateDESIV() ([]byte, error) {
	iv := make([]byte, LENGTH_OF_DES_COMMON_IV)

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, errors.Wrap(err, 0)
	}

	return iv, nil
}

func generate3DESIV() ([]byte, error) {
	iv := make([]byte, LENGTH_OF_3DES_COMMON_IV)

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, errors.Wrap(err, 0)
	}

	return iv, nil
}

func generateBLOWFISHIV() ([]byte, error) {
	iv := make([]byte, LENGTH_OF_BLOWFISH_COMMON_IV)

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, errors.Wrap(err, 0)
	}

	return iv, nil
}

func GenerateIVOrNonce(algo string, cryptoMode string) ([]byte, error) {
	switch algo {
	case "AES":
		{
			return generateAESIVOrNonce(cryptoMode)
		}
	case "DES":
		{
			return generateDESIV()
		}
	case "3DES":
		{
			return generate3DESIV()
		}
	case "BLOWFISH":
		{
			return generateBLOWFISHIV()
		}
	}
	return nil, nil
}
