//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package util

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/astaxie/beego"
	"github.com/tidwall/gjson"

	"git.forms.io/universe/comm-agent/client"
)

type FormHead struct {
	FormID string `json:"FormId"`
}

//type FormData map[string]interface{}

// type FormData struct {
// 	AcctNo                   string `json:"AcctNo"`
// 	AcctgAcctNo              string `json:"AcctgAcctNo"`
// 	Amt                      int    `json:"Amt"`
// 	BussAcctNo               string `json:"BussAcctNo"`
// 	BussCategories           string `json:"BussCategories"`
// 	BussDate                 string `json:"BussDate"`
// 	BussType                 string `json:"BussType"`
// 	Currency                 string `json:"Currency"`
// 	CustID                   string `json:"CustId"`
// 	DaysOfDep                int    `json:"DaysOfDep"`
// 	DurationOfDep            int    `json:"DurationOfDep"`
// 	FunctionCode             string `json:"FunctionCode"`
// 	IsMakeUpForPayables      string `json:"IsMakeUpForPayables"`
// 	IsSendClearingFlag       string `json:"IsSendClearingFlag"`
// 	LocalAcctDiff            string `json:"LocalAcctDiff"`
// 	LocalBankFlag            string `json:"LocalBankFlag"`
// 	LocalTranDetailType      string `json:"LocalTranDetailType"`
// 	OriginalHostTranSerialNo int    `json:"OriginalHostTranSerialNo"`
// 	PeripheralTranSeq        int    `json:"PeripheralTranSeq"`
// 	ProdCode                 string `json:"ProdCode"`
// 	ProdSeq                  int    `json:"ProdSeq"`
// 	SetNo                    string `json:"SetNo"`
// 	TrasactionCode           string `json:"TrasactionCode"`
// 	WhetherToAllowPunching   string `json:"WhetherToAllowPunching"`
// }

type Form1 struct {
	FormData map[string]interface{} `json:"FormData"`
	FromHead FormHead               `json:"FormHead"`
}
type Form2 struct {
	FormData []map[string]interface{} `json:"FormData"`
	FromHead FormHead                 `json:"FormHead"`
}
type FormData3 struct {
	LogTotCount string                   `json:"LogTotCount"`
	Records     []map[string]interface{} `json:"Records"`
}
type Form3 struct {
	FormData FormData3 `json:"FormData"`
	FromHead FormHead  `json:"FormHead"`
}

type Body1 struct {
	Form []struct {
		FormData map[string]interface{} `json:"FormData"`
		FormHead FormHead               `json:"FormHead"`
	} `json:"Form"`
}
type Body2 struct {
	Form []struct {
		FormData []map[string]interface{} `json:"FormData"`
		FormHead FormHead                 `json:"FormHead"`
	} `json:"Form"`
}
type Body3 struct {
	Form []struct {
		FormData struct {
			LogTotCount string                   `json:"LogTotCount"`
			Records     []map[string]interface{} `json:"Records"`
		} `json:"FormData"`
		FormHead FormHead `json:"FormHead"`
	} `json:"Form"`
}

type NewBody1 struct {
	Form []Form1 `json:"Form"`
}
type NewBody2 struct {
	Form []Form2 `json:"Form"`
}
type NewBody3 struct {
	Form []Form3 `json:"Form"`
}

type NewUserMessage struct {
	Id             uint64
	TopicAttribute map[string]string
	NeedReply      bool
	NeedAck        bool
	SessionName    string
	//app properties
	AppProps map[string]string //应用属性

	//message payload
	Body string
}

func MaskMsg(msg *client.UserMessage) NewUserMessage {
	keys := beego.AppConfig.DefaultString("log::maskKeys", "")
	newmsg := NewUserMessage{
		Id:             msg.Id,
		TopicAttribute: msg.TopicAttribute,
		NeedAck:        msg.NeedAck,
		NeedReply:      msg.NeedReply,
		SessionName:    msg.SessionName,
		AppProps:       msg.AppProps,
	}
	if keys == "" || len(keys) == 0 {
		newmsg.Body = string(msg.Body)
		return newmsg
	}
	logtype := getLogType(msg.Body)
	body, err := maskBody(strings.Split(keys, ","), msg.Body, logtype)
	if err != nil {
		fmt.Printf("failed==%s", err.Error())
		newmsg.Body = string(msg.Body)
		return newmsg
	}
	newmsg.Body = string(body)
	return newmsg
}

func getLogType(body []byte) string {
	value := gjson.Get(string(body), "Form").String()
	//fmt.Printf("value==%++v\n", value)
	if !strings.HasPrefix(value, "[") {
		//fmt.Println("msg format is error,logtype==")
		return ""
	}
	var a interface{}
	data := gjson.Get(value[1:len(value)-1], "FormData").String()

	gjson.Unmarshal([]byte(data), &a)

	switch a.(type) {
	case []interface{}:
		//fmt.Println("logtype==2")
		return "2"
	case interface{}:
		logtoacount := gjson.Get(data, "Records").String()
		if "" == logtoacount {
			//fmt.Println("logtype==1")
			return "1"
		}
		//fmt.Println("logtype==3")
		return "3"
	default:
		//fmt.Println("logtype==")
		return ""
	}
}

func maskBody(keys []string, body []byte, msgType string) (dst []byte, err error) {
	//fmt.Printf("%++v\n", keys)
	if keys == nil || len(keys) == 0 {
		return body, errors.New("mask field is null")
	}
	if msgType == "1" {
		return decodeType1(keys, body)
	} else if msgType == "2" {
		return decodeType2(keys, body)
	} else if msgType == "3" {
		return decodeType3(keys, body)
	} else {
		return body, errors.New("msg type is mismatch")
	}
}

func decodeType1(keys []string, body []byte) ([]byte, error) {
	var b Body1
	err := gjson.Unmarshal(body, &b)
	if err != nil {
		return nil, errors.New("unmarshal type1 is failed" + err.Error())
	}
	//fmt.Printf("old==%++v\n", b.Form)
	form1 := make([]Form1, 0)
	for _, form := range b.Form {
		data := make(map[string]interface{})
		for k, v := range form.FormData {
			data[k] = v
			for _, key := range keys {
				if key == k {
					newv := fmt.Sprintf("%v", v)
					//fmt.Println("newv==" + newv)
					data[k] = base64.StdEncoding.EncodeToString([]byte(newv))
					break
				}
			}
		}
		form := Form1{
			FormData: data,
			FromHead: form.FormHead,
		}
		form1 = append(form1, form)
	}
	//fmt.Printf("new==%++v\n", form1)
	return json.Marshal(&NewBody1{
		Form: form1,
	})
}

func decodeType2(keys []string, body []byte) ([]byte, error) {
	var b Body2
	err := gjson.Unmarshal(body, &b)
	if err != nil {
		return nil, errors.New("unmarshal type2 is failed" + err.Error())
	}
	//fmt.Printf("old==%++v\n", b.Form)
	form2 := make([]Form2, 0)
	for _, form := range b.Form {
		newformdata := make([]map[string]interface{}, 0)
		for _, formdata := range form.FormData {
			data := make(map[string]interface{})
			for k, v := range formdata {
				data[k] = v
				for _, key := range keys {
					if key == k {
						newv := fmt.Sprintf("%v", v)
						data[k] = base64.StdEncoding.EncodeToString([]byte(newv))
						break
					}
				}
			}
			newformdata = append(newformdata, data)
		}
		form := Form2{
			FormData: newformdata,
			FromHead: form.FormHead,
		}
		form2 = append(form2, form)
	}
	//fmt.Printf("new==%++v\n", form2)
	return json.Marshal(&NewBody2{
		Form: form2,
	})
}

func decodeType3(keys []string, body []byte) ([]byte, error) {
	var b Body3
	err := gjson.Unmarshal(body, &b)
	if err != nil {
		return nil, errors.New("unmarshal type3 is failed" + err.Error())
	}
	//fmt.Printf("old==%++v\n", b)
	form3 := make([]Form3, 0)
	for _, form := range b.Form {
		records := make([]map[string]interface{}, 0)
		for _, formdata := range form.FormData.Records {
			data := make(map[string]interface{})
			for k, v := range formdata {
				data[k] = v
				for _, key := range keys {
					if key == k {
						newv := fmt.Sprintf("%v", v)
						data[k] = base64.StdEncoding.EncodeToString([]byte(newv))
						break
					}
				}
			}
			records = append(records, data)
		}
		formdata3 := FormData3{
			LogTotCount: form.FormData.LogTotCount,
			Records:     records,
		}
		form := Form3{
			FormData: formdata3,
			FromHead: form.FormHead,
		}
		form3 = append(form3, form)
	}
	//fmt.Printf("new==%++v\n", form3)
	return json.Marshal(&NewBody3{
		Form: form3,
	})
}

func main1() {
	json1 := `{
		"Form": [
			{
				"FormHead": {
					"FormId": ""
				},
				"FormData": {
					"AcctNo": "105000000000001",
					"AcctgAcctNo": "105000000000001",
					"Amt": 10,
					"BussAcctNo": "105000000000001",
					"BussCategories": "1",
					"BussType": "1",
					"BussDate": "20190901",
					"Currency": "USD",
					"CustId": "111111",
					"DaysOfDep": 0,
					"DurationOfDep": 0,
					"FunctionCode": "1",
					"IsMakeUpForPayables": "N",
					"IsSendClearingFlag": "N",
					"LocalAcctDiff": "0",
					"LocalBankFlag": "1",
					"LocalTranDetailType": "1",
					"OriginalHostTranSerialNo": 0,
					"PeripheralTranSeq": 0,
					"ProdCode": "1",
					"ProdSeq": 2,
					"SetNo": "1",
					"TrasactionCode": "",
					"WhetherToAllowPunching": "N"
				}
			}
		]
	}`
	msg := &client.UserMessage{
		Id:             1,
		TopicAttribute: map[string]string{"name": "nanili"},
		NeedAck:        false,
		NeedReply:      false,
		SessionName:    "default",
		AppProps:       map[string]string{"logtype": "1"},
		Body:           []byte(json1),
	}
	newmsg := MaskMsg(msg)
	fmt.Printf("%++v", string(newmsg.Body))
}

func main2() {
	json2 := `{
		"Form": [
			{
				"FormHead": {
					"FormId": ""
				},
				"FormData": [
					{
						"AcctNo": "105000000000001",
						"AcctgAcctNo": "105000000000001",
						"Amt": 10,
						"BussAcctNo": "105000000000001",
						"BussCategories": "1",
						"BussType": "1",
						"BussDate": "20190901",
						"Currency": "USD",
						"CustId": "111111",
						"DaysOfDep": 0,
						"DurationOfDep": 0,
						"FunctionCode": "1",
						"IsMakeUpForPayables": "N",
						"IsSendClearingFlag": "N",
						"LocalAcctDiff": "0",
						"LocalBankFlag": "1",
						"LocalTranDetailType": "1",
						"OriginalHostTranSerialNo": 0,
						"PeripheralTranSeq": 0,
						"ProdCode": "1",
						"ProdSeq": 2,
						"SetNo": "1",
						"TrasactionCode": "",
						"WhetherToAllowPunching": "N"
					},
					{
						"AcctNo": "105000000000001",
						"AcctgAcctNo": "105000000000001",
						"Amt": 10,
						"BussAcctNo": "105000000000001",
						"BussCategories": "1",
						"BussType": "1",
						"BussDate": "20190901",
						"Currency": "USD",
						"CustId": "111111",
						"DaysOfDep": 0,
						"DurationOfDep": 0,
						"FunctionCode": "1",
						"IsMakeUpForPayables": "N",
						"IsSendClearingFlag": "N",
						"LocalAcctDiff": "0",
						"LocalBankFlag": "1",
						"LocalTranDetailType": "1",
						"OriginalHostTranSerialNo": 0,
						"PeripheralTranSeq": 0,
						"ProdCode": "1",
						"ProdSeq": 2,
						"SetNo": "1",
						"TrasactionCode": "",
						"WhetherToAllowPunching": "N"
					}
				]
			}
		]
	}`
	msg := &client.UserMessage{
		Id:             1,
		TopicAttribute: map[string]string{"name": "nanili"},
		NeedAck:        false,
		NeedReply:      false,
		SessionName:    "default",
		AppProps:       map[string]string{"logtype": "2"},
		Body:           []byte(json2),
	}
	newmsg := MaskMsg(msg)
	fmt.Printf("%++v", string(newmsg.Body))
}

func main3() {
	json3 := `{
		"Form": [
			{
				"FormHead": {
					"FormId": ""
				},
				"FormData": {
					"LogTotCount": "",
					"Records": [
						{
							"AcctNo": "105000000000001",
							"AcctgAcctNo": "105000000000001",
							"Amt": 10,
							"BussAcctNo": "105000000000001",
							"BussCategories": "1",
							"BussType": "1",
							"BussDate": "20190901",
							"Currency": "USD",
							"CustId": "111111",
							"DaysOfDep": 0,
							"DurationOfDep": 0,
							"FunctionCode": "1",
							"IsMakeUpForPayables": "N",
							"IsSendClearingFlag": "N",
							"LocalAcctDiff": "0",
							"LocalBankFlag": "1",
							"LocalTranDetailType": "1",
							"OriginalHostTranSerialNo": 0,
							"PeripheralTranSeq": 0,
							"ProdCode": "1",
							"ProdSeq": 2,
							"SetNo": "1",
							"TrasactionCode": "",
							"WhetherToAllowPunching": "N"
						},
						{
							"AcctNo": "105000000000001",
							"AcctgAcctNo": "105000000000001",
							"Amt": 10,
							"BussAcctNo": "105000000000001",
							"BussCategories": "1",
							"BussType": "1",
							"BussDate": "20190901",
							"Currency": "USD",
							"CustId": "111111",
							"DaysOfDep": 0,
							"DurationOfDep": 0,
							"FunctionCode": "1",
							"IsMakeUpForPayables": "N",
							"IsSendClearingFlag": "N",
							"LocalAcctDiff": "0",
							"LocalBankFlag": "1",
							"LocalTranDetailType": "1",
							"OriginalHostTranSerialNo": 0,
							"PeripheralTranSeq": 0,
							"ProdCode": "1",
							"ProdSeq": 2,
							"SetNo": "1",
							"TrasactionCode": "",
							"WhetherToAllowPunching": "N"
						}
					]
				}
			}
		]
	}`
	msg := &client.UserMessage{
		Id:             1,
		TopicAttribute: map[string]string{"name": "nanili"},
		NeedAck:        false,
		NeedReply:      false,
		SessionName:    "default",
		AppProps:       map[string]string{"logtype": "3"},
		Body:           []byte(json3),
	}
	newmsg := MaskMsg(msg)
	fmt.Printf("%++v", string(newmsg.Body))
}

func main() {
	main1()
	main2()
	main3()
}
