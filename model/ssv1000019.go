package model

type SSV1000019I struct {
	SysId             string            `json:"sysId" description:"Channel identification"`                        //渠道标识 暂定ILNS 乐高智能贷款
	BizSn             string            `json:"bizSn" description:"Business serial number"`                        //业务流水号
	ObjectSendList    IL0LSR01IItemList `json:"objectSendList" description:"Send object content collection"`       //发送对象内容集合
	ObjectReceiveList IL0LSR01IItemList `json:"objectReceiveList" description:"Receive object content collection"` //接收对象内容集合
	LoanProdtNo       string            `json:"loanProdtNo" description:"product ID"`                              //产品号
	ApplyScena        string            `json:"applyScena" description:"Applicable scenarios"`                     //适用场景大类
	TextTmplTypCd     string            `json:"textTmplTypCd" description:"Template business type code"`           //模板业务类型代码 1001-Transaction Statement 1002-Transaction History 1003-Repayment Plan 1004-Late Payment Notification
	OrgId             string            `json:"orgId" description:"Institution ID"`                                //机构id
	Area              string            `json:"area" description:"Area"`                                           //地区
	ChannelNumber     string            `json:"channelNumber" description:"Channel number"`                        //渠道号
	EventId           string            `json:"eventId" description:"Event ID"`                                    //事件ID TopicID
	SendObjectType    string            `json:"sendObjectType" description:"Send object type"`                     //发送对象类型（01-机构，02-客户经理）
	TextTmplDetailCd  string            `json:"textTmplDetailCd" description:"Template business type breakdown"`   //模板业务类型细类
	MsgDate           string            `json:"msgDate" description:"Date"`                                        //日期
	MsgTime           string            `json:"msgTime" description:"Time"`                                        //时间
	AttachFileIDList  []string          `json:"attachFileIDList" description:"Attachment file ID list"`            //附件文件ID列表
	Body              string            `json:"body" description:"content"`                                        //内容
}

type SSV1000019O struct {
	ReturnCode   string `json:"returnCode" description:"Error code"`          //错误码：0-邮件发送成功
	ReturnMsg    string `json:"returnMsg" description:"Response information"` //响应信息："SUCCESS"-发送成功
	DeliveryTime string `json:"deliveryTime" description:"Sending time"`      //发件时间，格式：YYYYMMDDHH24MMSS
	ResponseTime string `json:"responseTime" description:"response time"`     //响应时间，格式：YYYYMMDDHH24MMSS
	RepData      string `json:"repData" description:"Email results"`          //邮件发送结果：true-成功，false-失败
}

type IL0LSR01IItemList []IL0LSR01IItem

type IL0LSR01IItem struct {
	ObjectKeyType string `json:"objectKeyType" description:"Object key type"` // 对象键字类型 01-客户编号 02-账号 03-证件
	ObjectKey     string `json:"objectKey" description:"Object key word"`     // 对象键字 客户编号 账号 证件类型+证件号码
	SerialNo      int    `json:"serialNo"description:"Sort number"`           // 排序号
}
