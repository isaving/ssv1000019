//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000019/constant"
	"git.forms.io/legobank/legoapp/services"
)

type Ssv1000019Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Ssv100019O *models.SSV1000019O
	Ssv100019I *models.SSV1000019I
	il0lsr01O  *models.IL0LSR01O
}

// @Desc Ssv1000019 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000019Impl) Ssv1000019(ssv1000019I *models.SSV1000019I) (ssv1000019O *models.SSV1000019O, err error) {

	impl.Ssv100019I = ssv1000019I

	//调用动账邮件发送
	err = impl.SendEmail()
	if err != nil {
		return nil, err
	}

	ssv1000019O = &models.SSV1000019O{
		ReturnCode:   impl.il0lsr01O.ReturnCode,
		ReturnMsg:    impl.il0lsr01O.ReturnMsg,
		DeliveryTime: impl.il0lsr01O.DeliveryTime,
		ResponseTime: impl.il0lsr01O.ResponseTime,
		RepData:      impl.il0lsr01O.RepData,
	}

	return ssv1000019O, nil
}

//调用邮件发送
func (impl *Ssv1000019Impl) SendEmail() error {
	il0lsr01I := models.IL0LSR01I{
		SysId:             impl.Ssv100019I.SysId,
		BizSn:             impl.Ssv100019I.BizSn,
		ObjectSendList:    impl.Ssv100019I.ObjectSendList,
		ObjectReceiveList: impl.Ssv100019I.ObjectReceiveList,
		LoanProdtNo:       impl.Ssv100019I.LoanProdtNo,
		ApplyScena:        impl.Ssv100019I.ApplyScena,
		TextTmplTypCd:     impl.Ssv100019I.TextTmplTypCd,
		OrgId:             impl.Ssv100019I.OrgId,
		Area:              impl.Ssv100019I.Area,
		ChannelNumber:     impl.Ssv100019I.ChannelNumber,
		EventId:           impl.Ssv100019I.EventId,
		SendObjectType:    impl.Ssv100019I.SendObjectType,
		TextTmplDetailCd:  impl.Ssv100019I.TextTmplDetailCd,
		MsgDate:           impl.Ssv100019I.MsgDate,
		MsgTime:           impl.Ssv100019I.MsgTime,
		Body:              impl.Ssv100019I.Body,
		AttachFileIDList:  impl.Ssv100019I.AttachFileIDList,
	}
	il0lsr01Request := &models.IL0LSR01RequestForm{}
	requestBody, err := il0lsr01Request.PackRequest(il0lsr01I)
	if err != nil {
		return err
	}
	responseBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.IL0LSR01, requestBody)
	if err != nil {
		return err
	}
	il0lsr01Response := &models.IL0LSR01ResponseForm{}
	il0lsr01O, err := il0lsr01Response.UnPackResponse(responseBody)
	if err != nil {
		return err
	}
	impl.il0lsr01O = &il0lsr01O
	return nil
}
