//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var IL0LSR01 = `{
    "Form": [
        {
            "FormHead": {
                "FormID": ""
            },
            "FormData": {
				"returnCode":"",
				"returnMsg":"1",
				"deliveryTime":"1",
				"responseTime":"1",
				"repData":"1"

            }
        }
    ]
}`

func (this *Ssv1000019Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "IL0LSR01":
		responseData = []byte(IL0LSR01)
	}

	return responseData, nil
}

func TestSsv1000019Impl_Ssv1000020(t *testing.T) {

	Ssv1000019Impl := new(Ssv1000019Impl)

	_, _ = Ssv1000019Impl.Ssv1000019(&models.SSV1000019I{
		SysId:             "1",
		BizSn:             "1",
		ObjectSendList:    nil,
		ObjectReceiveList: nil,
		LoanProdtNo:       "1",
		ApplyScena:        "1",
		TextTmplTypCd:     "1",
		OrgId:             "1",
		Area:              "1",
		ChannelNumber:     "1",
		EventId:           "1",
		SendObjectType:    "1",
		TextTmplDetailCd:  "1",
		MsgDate:           "1",
		MsgTime:           "1",
		AttachFileIDList:  nil,
		Body:              nil,
	})
}
