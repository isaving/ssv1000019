//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000019/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000019Controller struct {
	controllers.CommController
}

func (*Ssv1000019Controller) ControllerName() string {
	return "Ssv1000019Controller"
}

// @Desc ssv1000019 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000019Controller) Ssv1000019() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000019Controller.Ssv1000019 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000019I := &models.SSV1000019I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000019I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000019I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000019 := &services.Ssv1000019Impl{}
	ssv1000019.New(c.CommController)
	ssv1000019.Ssv100019I = ssv1000019I

	ssv1000019O, err := ssv1000019.Ssv1000019(ssv1000019I)

	if err != nil {
		log.Errorf("Ssv1000019Controller.Ssv1000019 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000019O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// @Title Ssv1000019 Controller
// @Description ssv1000019 controller
// @Param Ssv1000019 body model.SSV1000019I true body for SSV1000019 content
// @Success 200 {object} model.SSV1000019O
// @router /create [post]
func (c *Ssv1000019Controller) SWSsv1000019() {
	//Here is to generate API documentation, no need to implement methods
}
